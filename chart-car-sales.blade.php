<div class="car-chart"
     wire:init="loadData"
>
    <div class="d-flex justify-content-between align-items-end">
        <div>
            <h3 class="car-subtitle mb-2">
            </h3>
            <h4 class="car-subsubtitle">
                @lang('Chart view')
            </h4>
        </div>
        @if(!empty($chartData['values']))
            <div wire:ignore id="chartCarControl">
                <div class="toggler">
                    <input wire:model="type" type="radio" class="d-none" name="type" value="min" id="btnMin"
                           autocomplete="off">
                    <label class="toggler-item" for="btnMin">Min</label>
                    <input wire:model="type" type="radio" class="d-none" name="type" value="avg" id="btnAvg"
                           autocomplete="off" checked>
                    <label class="toggler-item" for="btnAvg">Avg</label>
                    <input wire:model="type" type="radio" class="d-none" name="type" value="max" id="btnMax"
                           autocomplete="off">
                    <label class="toggler-item" for="btnMax">Max</label>
                    <input wire:model="type" type="radio" class="d-none" name="type" value="count" id="btnCount"
                           autocomplete="off">
                    <label class="toggler-item" for="btnCount">Count</label>
                </div>
            </div>
        @endif
    </div>
    <div id="chartCarDashboard" class="car-chart-wrap">
        @if(!empty($chartData['values']))
            <div id="chartCar" class="car-chart-chart"></div>
        @else
            <div wire:loading.block
                 wire:target="loadData"
                 wire:loading.class.remove="hidden"
                 class="hidden text-center mb-32 mt-32">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
            </div>
        @endif
    </div>
    <br>

    @if(!empty($chartData['values']))
        <div>
            <h3 class="car-subtitle mb-2">
                @lang('Table view')
            </h3>
        </div>

        <div id="chartCarDashboard" class="car-chart-wrap">

            <div id="chartCarTable" class="car-chart-chart">
                <table class="table table-striped table-hover">
                    @if(count($chartData['values']) == 2)
                        <thead>
                        <tr>
                            <th scope="col">Month</th>
                            <th scope="col" class="iaai">{{$chartData['auction_name'][0]['auction_name']}}</th>
                            <th scope="col" class="copart">{{$chartData['auction_name'][1]['auction_name']}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($chartData['type_values'] as $key => $item)
                            <tr>
                                <td>{{date('F', mktime(0, 0, 0, $item[0], 10))}}</td>
                                <td class="iaai">{{$item[1][0] . $add}}</td>
                                <td class="copart">{{$item[1][1] . $add}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    @endif
                    @if(count($chartData['values']) == 1)
                        <thead>
                        <tr>
                            <th scope="col">Month</th>
                            <th scope="col">{{$chartData['auction_name'][0]['auction_name']}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($chartData['type_values'] as $item)
                            <tr>
                                <td>{{date('F', mktime(0, 0, 0, $item[0], 10))}}</td>
                                <td class="iaai">{{$item[1] . $add}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    @endif
                </table>
            </div>
        </div>
    @else
        <div wire:loading.block
             wire:target="loadData"
             wire:loading.class.remove="hidden"
             class="hidden text-center mb-32 mt-32">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        </div>
    @endif


    @push('scripts')
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="{{ asset('/assets/js/charts/car-chart.js') }}"></script>
    @endpush
</div>
