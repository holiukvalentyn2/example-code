<?php

namespace App\Services\ChartCarSales\Repositories;

use App\Models\SaleStatisticsByPeriod;
use App\Models\Auctions;
use Illuminate\Support\Facades\Cache;

class ChartCarSalesRepository implements ChartCarSalesRepositoryInterface
{
	public function show($make, $model, $year, $dates, $auction_code)
	{
		$cashName = $make . '_' . $model . '_' . $year . '_' . $dates['dateFrom'] . '_' . $dates['dateTo'] . '_' . $auction_code;

		return Cache::remember($cashName, 259200, function () use ($make, $model, $year, $dates, $auction_code) {
			return SaleStatisticsByPeriod::query()
				->whereHas('make', function ($rq) use ($make) {
					$rq->where('make', $make);
				})
				->whereHas('model', function ($rq) use ($model) {
					$rq->where('model', $model);
				})
				->where('car_year', $year)
				->whereBetween('date', [
					$dates['dateFrom'],
					$dates['dateTo'],
				])->get();
		});
	}

    public function getAllAuctionNames($auction_id)
    {
            return Auctions::query()
                ->whereIn('id', $auction_id)
                ->get();
    }

    public function getAllTypeValues($make, $model, $year, $dates, $type)
    {
        return SaleStatisticsByPeriod::query()
                ->select([$type, 'id', 'make_id', 'model_id', 'auction'])
                ->whereHas('make', function ($rq) use ($make) {
                    $rq->where('make', $make);
                })
                ->whereHas('model', function ($rq) use ($model) {
                    $rq->where('model', $model);
                })
                ->where('car_year', $year)
                ->whereBetween('date', [
                    $dates['dateFrom'],
                    $dates['dateTo'],
                ])
                ->get();
    }
}
