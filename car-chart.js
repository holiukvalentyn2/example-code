/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/
(() => { // webpackBootstrap
    /******/
    var __webpack_modules__ = ({

        /***/ "./resources/js/charts/car-chart.js":
        /*!******************************************!*\
          !*** ./resources/js/charts/car-chart.js ***!
          \******************************************/
        /***/ (() => {

            // eval("function initChartCar(chartData, chartTitles, chartAuctionsNames) {\n  console.log('done');\n  if (chartData) {\n    var drawChart = function drawChart() {\n      var data = new google.visualization.DataTable();\n      chartAuctionsNames.forEach(f);\n      data.addColumn('string', 'Date');\n      data.addColumn('number', 'IAAI');\n      data.addColumn({\n        type: 'string',\n        role: 'tooltip',\n        'p': {\n          'html': true\n        }\n      });\n      data.addColumn('number', 'COPART');\n      data.addColumn({\n        type: 'string',\n        role: 'tooltip',\n        'p': {\n          'html': true\n        }\n      });\n      data.addRows(formatChartData(chartData, chartTitles.type));\n      var options = {\n        title: chartTitles.make + ' ' + chartTitles.model + ' ' + chartTitles.year + ' - sale statistic',\n        pointSize: 7,\n        hAxis: {\n          minValue: 0,\n          title: 'Date'\n        },\n        vAxis: {\n          minValue: 0,\n          title: 'Price, $',\n          viewWindow: {\n            min: 0\n          }\n        },\n        curveType: 'function',\n        theme: 'material',\n        colors: ['#db4437', '#5e97f6'],\n        tooltip: {\n          isHtml: true\n        }\n      };\n      var chart = new google.visualization.LineChart(document.getElementById('chartCar'));\n      chart.draw(data, options);\n    };\n    google.charts.load('current', {\n      'packages': ['corechart', 'line']\n    });\n    google.charts.setOnLoadCallback(drawChart);\n  }\n}\nfunction formatChartData(chartData, chartTitlesType) {\n  var formatChartData = [];\n  var statType = '';\n  switch (chartTitlesType) {\n    case \"avg\":\n      statType = 'middle_bid';\n      break;\n    case \"min\":\n      statType = 'min_bid';\n      break;\n    case \"max\":\n      statType = 'max_bid';\n      break;\n  }\n  for (var iMonth = 0; iMonth <= 11; iMonth++) {\n    formatChartData[iMonth] = [chartData[1][iMonth].statistic_year + '-' + chartData[1][iMonth].statistic_month, chartData[1][iMonth][statType], '<div class=\"chart-tooltip\"><span><b class=\"iaai\">IAAI</b>: ' + formatPrice(chartData[1][iMonth][statType]) + '<br><b class=\"copart\">COPART</b>: ' + formatPrice(chartData[2][iMonth][statType]) + '</span></div>', chartData[2][iMonth][statType], '<div class=\"chart-tooltip\"><span><b class=\"copart\">COPART</b>: ' + formatPrice(chartData[2][iMonth][statType]) + '<br><b class=\"iaai\">IAAI</b>: ' + formatPrice(chartData[1][iMonth][statType]) + '</span></div>'];\n  }\n  return formatChartData;\n}\nfunction formatPrice(price) {\n  return price.toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, \" \") + '$';\n}\nwindow.addEventListener('init-chart-car', function (event) {\n  initChartCar(event.detail.chartValues, event.detail.chartTitles, event.detail.chartAuctionsNames);\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvY2hhcnRzL2Nhci1jaGFydC5qcy5qcyIsIm5hbWVzIjpbImluaXRDaGFydENhciIsImNoYXJ0RGF0YSIsImNoYXJ0VGl0bGVzIiwiY2hhcnRBdWN0aW9uc05hbWVzIiwiY29uc29sZSIsImxvZyIsImRyYXdDaGFydCIsImRhdGEiLCJnb29nbGUiLCJ2aXN1YWxpemF0aW9uIiwiRGF0YVRhYmxlIiwiZm9yRWFjaCIsImYiLCJhZGRDb2x1bW4iLCJ0eXBlIiwicm9sZSIsImFkZFJvd3MiLCJmb3JtYXRDaGFydERhdGEiLCJvcHRpb25zIiwidGl0bGUiLCJtYWtlIiwibW9kZWwiLCJ5ZWFyIiwicG9pbnRTaXplIiwiaEF4aXMiLCJtaW5WYWx1ZSIsInZBeGlzIiwidmlld1dpbmRvdyIsIm1pbiIsImN1cnZlVHlwZSIsInRoZW1lIiwiY29sb3JzIiwidG9vbHRpcCIsImlzSHRtbCIsImNoYXJ0IiwiTGluZUNoYXJ0IiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsImRyYXciLCJjaGFydHMiLCJsb2FkIiwic2V0T25Mb2FkQ2FsbGJhY2siLCJjaGFydFRpdGxlc1R5cGUiLCJzdGF0VHlwZSIsImlNb250aCIsInN0YXRpc3RpY195ZWFyIiwic3RhdGlzdGljX21vbnRoIiwiZm9ybWF0UHJpY2UiLCJwcmljZSIsInRvU3RyaW5nIiwicmVwbGFjZSIsIndpbmRvdyIsImFkZEV2ZW50TGlzdGVuZXIiLCJldmVudCIsImRldGFpbCIsImNoYXJ0VmFsdWVzIl0sInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvY2hhcnRzL2Nhci1jaGFydC5qcz9kN2EzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIGluaXRDaGFydENhcihjaGFydERhdGEsIGNoYXJ0VGl0bGVzLCBjaGFydEF1Y3Rpb25zTmFtZXMpIHtcbiAgICBjb25zb2xlLmxvZygnZG9uZScpO1xuICAgIGlmIChjaGFydERhdGEpIHtcbiAgICAgICAgZ29vZ2xlLmNoYXJ0cy5sb2FkKCdjdXJyZW50JywgeydwYWNrYWdlcyc6Wydjb3JlY2hhcnQnLCAnbGluZSddfSk7XG4gICAgICAgIGdvb2dsZS5jaGFydHMuc2V0T25Mb2FkQ2FsbGJhY2soZHJhd0NoYXJ0KTtcblxuICAgICAgICBmdW5jdGlvbiBkcmF3Q2hhcnQoKSB7XG4gICAgICAgICAgICBsZXQgZGF0YSA9IG5ldyBnb29nbGUudmlzdWFsaXphdGlvbi5EYXRhVGFibGUoKTtcblxuICAgICAgICAgICAgY2hhcnRBdWN0aW9uc05hbWVzLmZvckVhY2goZik7XG4gICAgICAgICAgICBkYXRhLmFkZENvbHVtbignc3RyaW5nJywgJ0RhdGUnKTtcbiAgICAgICAgICAgIGRhdGEuYWRkQ29sdW1uKCdudW1iZXInLCAnSUFBSScpO1xuICAgICAgICAgICAgZGF0YS5hZGRDb2x1bW4oe3R5cGU6ICdzdHJpbmcnLCByb2xlOiAndG9vbHRpcCcsICdwJzogeydodG1sJzogdHJ1ZX19KTtcbiAgICAgICAgICAgIGRhdGEuYWRkQ29sdW1uKCdudW1iZXInLCAnQ09QQVJUJyk7XG4gICAgICAgICAgICBkYXRhLmFkZENvbHVtbih7dHlwZTogJ3N0cmluZycsIHJvbGU6ICd0b29sdGlwJywgJ3AnOiB7J2h0bWwnOiB0cnVlfX0pO1xuXG4gICAgICAgICAgICBkYXRhLmFkZFJvd3MoZm9ybWF0Q2hhcnREYXRhKGNoYXJ0RGF0YSwgY2hhcnRUaXRsZXMudHlwZSkpO1xuXG4gICAgICAgICAgICBsZXQgb3B0aW9ucyA9IHtcbiAgICAgICAgICAgICAgICB0aXRsZTogY2hhcnRUaXRsZXMubWFrZSsnICcrY2hhcnRUaXRsZXMubW9kZWwrJyAnK2NoYXJ0VGl0bGVzLnllYXIrJyAtIHNhbGUgc3RhdGlzdGljJyxcbiAgICAgICAgICAgICAgICBwb2ludFNpemU6IDcsXG4gICAgICAgICAgICAgICAgaEF4aXM6IHsgbWluVmFsdWU6IDAsIHRpdGxlOiAnRGF0ZSd9LFxuICAgICAgICAgICAgICAgIHZBeGlzOiB7IG1pblZhbHVlOiAwLCB0aXRsZTogJ1ByaWNlLCAkJyx2aWV3V2luZG93OiB7bWluOiAwfX0sXG4gICAgICAgICAgICAgICAgY3VydmVUeXBlOiAnZnVuY3Rpb24nLFxuICAgICAgICAgICAgICAgIHRoZW1lOiAnbWF0ZXJpYWwnLFxuICAgICAgICAgICAgICAgIGNvbG9yczogWycjZGI0NDM3JywgJyM1ZTk3ZjYnXSxcbiAgICAgICAgICAgICAgICB0b29sdGlwOiB7IGlzSHRtbDogdHJ1ZSB9XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgbGV0IGNoYXJ0ID0gbmV3IGdvb2dsZS52aXN1YWxpemF0aW9uLkxpbmVDaGFydChkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY2hhcnRDYXInKSk7XG4gICAgICAgICAgICBjaGFydC5kcmF3KGRhdGEsIG9wdGlvbnMpO1xuICAgICAgICB9XG4gICAgfVxufVxuXG5mdW5jdGlvbiBmb3JtYXRDaGFydERhdGEoY2hhcnREYXRhLCBjaGFydFRpdGxlc1R5cGUpIHtcbiAgICBsZXQgZm9ybWF0Q2hhcnREYXRhID0gW107XG5cbiAgICBsZXQgc3RhdFR5cGUgPSAnJztcbiAgICBzd2l0Y2ggKGNoYXJ0VGl0bGVzVHlwZSkge1xuICAgICAgICBjYXNlIFwiYXZnXCI6XG4gICAgICAgICAgICBzdGF0VHlwZSA9ICdtaWRkbGVfYmlkJztcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIFwibWluXCI6XG4gICAgICAgICAgICBzdGF0VHlwZSA9ICdtaW5fYmlkJztcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIFwibWF4XCI6XG4gICAgICAgICAgICBzdGF0VHlwZSA9ICdtYXhfYmlkJztcbiAgICAgICAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIGZvciAobGV0IGlNb250aCA9IDA7IGlNb250aCA8PSAxMTsgaU1vbnRoKyspIHtcbiAgICAgICAgZm9ybWF0Q2hhcnREYXRhW2lNb250aF0gPSBbXG4gICAgICAgICAgICBjaGFydERhdGFbMV1baU1vbnRoXS5zdGF0aXN0aWNfeWVhcisnLScrY2hhcnREYXRhWzFdW2lNb250aF0uc3RhdGlzdGljX21vbnRoLFxuICAgICAgICAgICAgY2hhcnREYXRhWzFdW2lNb250aF1bc3RhdFR5cGVdLFxuICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJjaGFydC10b29sdGlwXCI+PHNwYW4+PGIgY2xhc3M9XCJpYWFpXCI+SUFBSTwvYj46ICcgKyBmb3JtYXRQcmljZShjaGFydERhdGFbMV1baU1vbnRoXVtzdGF0VHlwZV0pICsgJzxicj48YiBjbGFzcz1cImNvcGFydFwiPkNPUEFSVDwvYj46ICcgKyBmb3JtYXRQcmljZShjaGFydERhdGFbMl1baU1vbnRoXVtzdGF0VHlwZV0pICsgJzwvc3Bhbj48L2Rpdj4nLFxuICAgICAgICAgICAgY2hhcnREYXRhWzJdW2lNb250aF1bc3RhdFR5cGVdLFxuICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJjaGFydC10b29sdGlwXCI+PHNwYW4+PGIgY2xhc3M9XCJjb3BhcnRcIj5DT1BBUlQ8L2I+OiAnICsgZm9ybWF0UHJpY2UoY2hhcnREYXRhWzJdW2lNb250aF1bc3RhdFR5cGVdKSArICc8YnI+PGIgY2xhc3M9XCJpYWFpXCI+SUFBSTwvYj46ICcgKyBmb3JtYXRQcmljZShjaGFydERhdGFbMV1baU1vbnRoXVtzdGF0VHlwZV0pICsgJzwvc3Bhbj48L2Rpdj4nXG4gICAgICAgIF07XG4gICAgfVxuXG4gICAgcmV0dXJuIGZvcm1hdENoYXJ0RGF0YTtcbn1cblxuZnVuY3Rpb24gZm9ybWF0UHJpY2UocHJpY2UpIHtcbiAgICByZXR1cm4gcHJpY2UudG9TdHJpbmcoKS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIiBcIikgKyAnJCc7XG59XG5cbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdpbml0LWNoYXJ0LWNhcicsIGV2ZW50ID0+IHtcbiAgICBpbml0Q2hhcnRDYXIoZXZlbnQuZGV0YWlsLmNoYXJ0VmFsdWVzLCBldmVudC5kZXRhaWwuY2hhcnRUaXRsZXMsIGV2ZW50LmRldGFpbC5jaGFydEF1Y3Rpb25zTmFtZXMpO1xufSk7XG4iXSwibWFwcGluZ3MiOiJBQUFBLFNBQVNBLFlBQVksQ0FBQ0MsU0FBUyxFQUFFQyxXQUFXLEVBQUVDLGtCQUFrQixFQUFFO0VBQzlEQyxPQUFPLENBQUNDLEdBQUcsQ0FBQyxNQUFNLENBQUM7RUFDbkIsSUFBSUosU0FBUyxFQUFFO0lBQUEsSUFJRkssU0FBUyxHQUFsQixTQUFTQSxTQUFTLEdBQUc7TUFDakIsSUFBSUMsSUFBSSxHQUFHLElBQUlDLE1BQU0sQ0FBQ0MsYUFBYSxDQUFDQyxTQUFTLEVBQUU7TUFFL0NQLGtCQUFrQixDQUFDUSxPQUFPLENBQUNDLENBQUMsQ0FBQztNQUM3QkwsSUFBSSxDQUFDTSxTQUFTLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQztNQUNoQ04sSUFBSSxDQUFDTSxTQUFTLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQztNQUNoQ04sSUFBSSxDQUFDTSxTQUFTLENBQUM7UUFBQ0MsSUFBSSxFQUFFLFFBQVE7UUFBRUMsSUFBSSxFQUFFLFNBQVM7UUFBRSxHQUFHLEVBQUU7VUFBQyxNQUFNLEVBQUU7UUFBSTtNQUFDLENBQUMsQ0FBQztNQUN0RVIsSUFBSSxDQUFDTSxTQUFTLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQztNQUNsQ04sSUFBSSxDQUFDTSxTQUFTLENBQUM7UUFBQ0MsSUFBSSxFQUFFLFFBQVE7UUFBRUMsSUFBSSxFQUFFLFNBQVM7UUFBRSxHQUFHLEVBQUU7VUFBQyxNQUFNLEVBQUU7UUFBSTtNQUFDLENBQUMsQ0FBQztNQUV0RVIsSUFBSSxDQUFDUyxPQUFPLENBQUNDLGVBQWUsQ0FBQ2hCLFNBQVMsRUFBRUMsV0FBVyxDQUFDWSxJQUFJLENBQUMsQ0FBQztNQUUxRCxJQUFJSSxPQUFPLEdBQUc7UUFDVkMsS0FBSyxFQUFFakIsV0FBVyxDQUFDa0IsSUFBSSxHQUFDLEdBQUcsR0FBQ2xCLFdBQVcsQ0FBQ21CLEtBQUssR0FBQyxHQUFHLEdBQUNuQixXQUFXLENBQUNvQixJQUFJLEdBQUMsbUJBQW1CO1FBQ3RGQyxTQUFTLEVBQUUsQ0FBQztRQUNaQyxLQUFLLEVBQUU7VUFBRUMsUUFBUSxFQUFFLENBQUM7VUFBRU4sS0FBSyxFQUFFO1FBQU0sQ0FBQztRQUNwQ08sS0FBSyxFQUFFO1VBQUVELFFBQVEsRUFBRSxDQUFDO1VBQUVOLEtBQUssRUFBRSxVQUFVO1VBQUNRLFVBQVUsRUFBRTtZQUFDQyxHQUFHLEVBQUU7VUFBQztRQUFDLENBQUM7UUFDN0RDLFNBQVMsRUFBRSxVQUFVO1FBQ3JCQyxLQUFLLEVBQUUsVUFBVTtRQUNqQkMsTUFBTSxFQUFFLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQztRQUM5QkMsT0FBTyxFQUFFO1VBQUVDLE1BQU0sRUFBRTtRQUFLO01BQzVCLENBQUM7TUFDRCxJQUFJQyxLQUFLLEdBQUcsSUFBSTFCLE1BQU0sQ0FBQ0MsYUFBYSxDQUFDMEIsU0FBUyxDQUFDQyxRQUFRLENBQUNDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztNQUNuRkgsS0FBSyxDQUFDSSxJQUFJLENBQUMvQixJQUFJLEVBQUVXLE9BQU8sQ0FBQztJQUM3QixDQUFDO0lBM0JEVixNQUFNLENBQUMrQixNQUFNLENBQUNDLElBQUksQ0FBQyxTQUFTLEVBQUU7TUFBQyxVQUFVLEVBQUMsQ0FBQyxXQUFXLEVBQUUsTUFBTTtJQUFDLENBQUMsQ0FBQztJQUNqRWhDLE1BQU0sQ0FBQytCLE1BQU0sQ0FBQ0UsaUJBQWlCLENBQUNuQyxTQUFTLENBQUM7RUEyQjlDO0FBQ0o7QUFFQSxTQUFTVyxlQUFlLENBQUNoQixTQUFTLEVBQUV5QyxlQUFlLEVBQUU7RUFDakQsSUFBSXpCLGVBQWUsR0FBRyxFQUFFO0VBRXhCLElBQUkwQixRQUFRLEdBQUcsRUFBRTtFQUNqQixRQUFRRCxlQUFlO0lBQ25CLEtBQUssS0FBSztNQUNOQyxRQUFRLEdBQUcsWUFBWTtNQUN2QjtJQUNKLEtBQUssS0FBSztNQUNOQSxRQUFRLEdBQUcsU0FBUztNQUNwQjtJQUNKLEtBQUssS0FBSztNQUNOQSxRQUFRLEdBQUcsU0FBUztNQUNwQjtFQUFNO0VBR2QsS0FBSyxJQUFJQyxNQUFNLEdBQUcsQ0FBQyxFQUFFQSxNQUFNLElBQUksRUFBRSxFQUFFQSxNQUFNLEVBQUUsRUFBRTtJQUN6QzNCLGVBQWUsQ0FBQzJCLE1BQU0sQ0FBQyxHQUFHLENBQ3RCM0MsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDMkMsTUFBTSxDQUFDLENBQUNDLGNBQWMsR0FBQyxHQUFHLEdBQUM1QyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMyQyxNQUFNLENBQUMsQ0FBQ0UsZUFBZSxFQUM1RTdDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzJDLE1BQU0sQ0FBQyxDQUFDRCxRQUFRLENBQUMsRUFDOUIsNkRBQTZELEdBQUdJLFdBQVcsQ0FBQzlDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzJDLE1BQU0sQ0FBQyxDQUFDRCxRQUFRLENBQUMsQ0FBQyxHQUFHLG9DQUFvQyxHQUFHSSxXQUFXLENBQUM5QyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMyQyxNQUFNLENBQUMsQ0FBQ0QsUUFBUSxDQUFDLENBQUMsR0FBRyxlQUFlLEVBQ2xOMUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDMkMsTUFBTSxDQUFDLENBQUNELFFBQVEsQ0FBQyxFQUM5QixpRUFBaUUsR0FBR0ksV0FBVyxDQUFDOUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDMkMsTUFBTSxDQUFDLENBQUNELFFBQVEsQ0FBQyxDQUFDLEdBQUcsZ0NBQWdDLEdBQUdJLFdBQVcsQ0FBQzlDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzJDLE1BQU0sQ0FBQyxDQUFDRCxRQUFRLENBQUMsQ0FBQyxHQUFHLGVBQWUsQ0FDck47RUFDTDtFQUVBLE9BQU8xQixlQUFlO0FBQzFCO0FBRUEsU0FBUzhCLFdBQVcsQ0FBQ0MsS0FBSyxFQUFFO0VBQ3hCLE9BQU9BLEtBQUssQ0FBQ0MsUUFBUSxFQUFFLENBQUNDLE9BQU8sQ0FBQyx1QkFBdUIsRUFBRSxHQUFHLENBQUMsR0FBRyxHQUFHO0FBQ3ZFO0FBRUFDLE1BQU0sQ0FBQ0MsZ0JBQWdCLENBQUMsZ0JBQWdCLEVBQUUsVUFBQUMsS0FBSyxFQUFJO0VBQy9DckQsWUFBWSxDQUFDcUQsS0FBSyxDQUFDQyxNQUFNLENBQUNDLFdBQVcsRUFBRUYsS0FBSyxDQUFDQyxNQUFNLENBQUNwRCxXQUFXLEVBQUVtRCxLQUFLLENBQUNDLE1BQU0sQ0FBQ25ELGtCQUFrQixDQUFDO0FBQ3JHLENBQUMsQ0FBQyJ9\n//# sourceURL=webpack-internal:///./resources/js/charts/car-chart.js\n");

            function initChartCar(chartData, chartTitles, chartAuctionsNames) {

                if (chartData) {
                    google.charts.load('current', {'packages': ['corechart','line']});
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        let data = new google.visualization.DataTable();
                        data.addColumn('string', 'Date');

                        chartAuctionsNames.forEach(function callback(currentValue) {
                            data.addColumn('number', currentValue.auction_name);
                            data.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
                        });

                        data.addRows(formatChartData(chartData, chartTitles.type, chartAuctionsNames));

                        let options = {
                            title: chartTitles.make + ' ' + chartTitles.model + ' ' + chartTitles.year + ' - sale statistic',
                            pointSize: 7,
                            hAxis: {minValue: 0, title: 'Date'},
                            vAxis: {minValue: 0, title: 'Price, $', viewWindow: {min: 0}},
                            curveType: 'function',
                            theme: 'material',
                            colors: ['#db4437', '#5e97f6'],
                            tooltip: {isHtml: true}
                        };

                        let chart = new google.visualization.LineChart(document.getElementById('chartCar'));
                        chart.draw(data, options);
                    }
                }
            }

            function formatChartData(chartData, chartTitlesType, chartAuctionsNames) {
                let formatChartData = [];

                let statType = '';
                switch (chartTitlesType) {
                    case "avg":
                        statType = 'middle_bid';
                        break;
                    case "min":
                        statType = 'min_bid';
                        break;
                    case "max":
                        statType = 'max_bid';
                        break;
                    case "count":
                        statType = 'count_sales';
                        break;
                }

                for (let iMonth = 0; iMonth <= 11; iMonth++) {

                    formatChartData[iMonth] = [
                        chartData[1][iMonth].statistic_year + '-' + chartData[1][iMonth].statistic_month,
                    ];

                    if (chartAuctionsNames.length == 2) {
                        formatChartData[iMonth].push(chartData[1][iMonth][statType]);
                        formatChartData[iMonth].push('<div class="chart-tooltip"><span><b class="iaai">' + chartAuctionsNames[0]['auction_name'] + '</b>: ' + formatPrice(chartData[1][iMonth][statType], statType) + '<br><b class="copart">' + chartAuctionsNames[1]['auction_name'] + '</b>: ' + formatPrice(chartData[2][iMonth][statType])  + '</span></div>');
                        formatChartData[iMonth].push(chartData[2][iMonth][statType]);
                        formatChartData[iMonth].push('<div class="chart-tooltip"><span><b class="copart">' + chartAuctionsNames[1]['auction_name'] + '</b>: ' + formatPrice(chartData[2][iMonth][statType], statType) + '<br><b class="iaai">' + chartAuctionsNames[0]['auction_name'] + '</b>: ' + formatPrice(chartData[1][iMonth][statType])  + '</span></div>');
                    } else {
                        formatChartData[iMonth].push(chartData[1][iMonth][statType]);
                        formatChartData[iMonth].push('<div class="chart-tooltip"><span><b class="iaai">' + chartAuctionsNames[0]['auction_name'] + '</b>: ' + formatPrice(chartData[1][iMonth][statType], statType) + '</span></div>');
                    }
                }
                return formatChartData;
            }

            function formatPrice(price, statType) {
                console.log(statType);
                if (statType === 'count_sales') {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                } else {
                    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + '$';
                }
            }

            window.addEventListener('init-chart-car', event => {
                initChartCar(event.detail.chartData, event.detail.chartTitles, event.detail.chartAuctionsNames);
            });


            /***/
        })

        /******/
    });
    /************************************************************************/
    /******/
    /******/ 	// startup
    /******/ 	// Load entry module and return exports
    /******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
    /******/
    var __webpack_exports__ = {};
    /******/
    __webpack_modules__["./resources/js/charts/car-chart.js"]();
    /******/
    /******/
})()
;
