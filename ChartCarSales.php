<?php

namespace App\Http\Livewire;

use App\Services\ChartCarSales\ChartCarSalesService;
use Carbon\Carbon;
use Livewire\Component;

class ChartCarSales extends Component
{
    private $chartCarSalesService;

    public bool $readyToLoad = false;
    public ?string $make = null;
    public ?string $model = null;
    public string $dateFrom = '';
    public string $dateTo = '';
    public string $type = 'avg';
    public string $add = '$';
    public ?int $year = null;
    public ?int $auction_code = null;
    public ?int $auction_id = null;

    public array $dates = [];
    public array $chartData = [];

    public function __construct()
    {
        parent::__construct();
        $this->chartCarSalesService = app(ChartCarSalesService::class);
    }

    public function mount( $make = null, $model = null, $year = null, $auction_code = null, $auction_id = null)
    {
        $this->make              = $make;
        $this->model             = $model;
        $this->year              = $year;
        $this->auction_code      = $auction_code;
        $this->auction_id        = $auction_id;

        $this->chartData['titles'] = ['make' => $make, 'model' => $model, 'year' => $year, 'type' => $this->type];

        $this->dateFrom = $dateFrom = Carbon::now()->subMonths(12);
        $this->dateTo   = $dateTo = Carbon::now()->subMonths();
        $this->dates = ['from' => ['year' => $dateFrom->year, 'month' => $dateFrom->month],
                        'to' => ['year' => $dateTo->year, 'month' => $dateTo->month]];
    }

    public function render()
    {
//        if ($this->readyToLoad) {
        $this->getChartData();
//            dd($this->chartData);
//        }

        return view('livewire.chart-car-sales', ['add' => $this->add]);
    }

    public function loadData()
    {
        $this->readyToLoad = true;
    }

    private function getChartData()
    {
        if ($this->make && $this->model && $this->year && $this->dates && $this->auction_id && $this->type) {

            $this->chartData['titles']['type'] = $this->type;

            if (empty($this->chartData['values'])) {
                $this->chartData['values'] = $this->chartCarSalesService->show($this->make, $this->model, $this->year, $this->dates, $this->auction_code);
            }

            if ($this->auction_id == 1 || $this->auction_id == 2) {
                $auction_id = [1, 2];
            } else {
                $auction_id = [$this->auction_id];
            }
            $this->chartData['auction_name'] = $this->chartCarSalesService->getAllAuctionNames($auction_id);

            $statType  = '';
            switch ($this->type) {
                case "avg":$statType = 'middle_bid';
                    break;
                case "min":$statType = 'min_bid';
                    break;
                case "max":$statType = 'max_bid';
                    break;
                case "count":$statType = 'count_sales';
                    break;
            }
            if ($statType == 'count_sales') {$this->add = '';}

            $collection = collect($this->chartData['values'])->flatten(1);

            $groupedType = collect($this->type($collection, $statType))->flatten(1);
            $groupedMonth = collect($this->month($collection))->flatten(1);

            if ($collection->count()/2 >= 12){
                $sliceMonth = $groupedMonth->slice($groupedMonth->count()/2)->skip($groupedMonth->count()/2 - 12)->values();
                $sliceType = $groupedType->splice($groupedType->count()/2)->skip($collection->count()/2 - 12);
                $groupedType = $groupedType->skip($collection->count()/2 - 12);

                $zipAuctionsType = $groupedType->zip($sliceType);
                $zipTypeData = $sliceMonth->zip($zipAuctionsType)->toArray();

            }if ($collection->count()/2 < 12){
                $zipTypeData = $groupedMonth->zip($groupedType)->toArray();
            }
                $this->chartData['type_values'] = $zipTypeData;

            $this->dispatchBrowserEvent('init-chart-car',
                ['chartData' => $this->chartData['values'],
                 'chartTitles' => $this->chartData['titles'],
                 'chartAuctionsNames' => $this->chartData['auction_name']
                ]);
        }
    }

    private function month($collection){
        $groupedMonth = $collection->mapToGroups(function ($item) {
            return [$item['car_year'] => $item['statistic_month']];
        });
        return $groupedMonth;
    }
    private function type($collection, $statType){
        $groupedType = $collection->mapToGroups(function ($item) use ($statType) {
            return [$item['car_year'] => $item[$statType]];
        });
        return $groupedType;
    }
}
