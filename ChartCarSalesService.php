<?php

namespace App\Services\ChartCarSales;

use App\Services\ChartCarSales\Repositories\ChartCarSalesRepositoryInterface;

class ChartCarSalesService
{
    private $repository;

    public function __construct()
    {
        $this->repository = app(ChartCarSalesRepositoryInterface::class);
    }

    public function getAllTypeValues(string $make, string $model, int $year, array $dates, string $type, int $auction_code) : array
    {
        $typeValues = $this->repository->getAllTypeValues($make, $model, $year, $this->formatDates($dates), $type);
        return $this->formatTypeData($typeValues, $dates, $auction_code);
    }
    private function formatTypeData($typeValues, $dates, $auction_code): array
    {
        $formatTypeData = [];

        if ($typeValues->count()) {
            if ($typeValues->count() != 24) {
                for ($y = $dates['from']['year']; $y <= $dates['to']['year'];) {
                    $startMonth = $y == $dates['from']['year'] ? $dates['from']['month'] : 1;

                    for ($m = $startMonth; $m <= 12; $m++) {
                        $date = $y . $this->formatMonth($m);

                        if ($auction_code == 1 || $auction_code == 2 ){
                            $typeItem = $typeValues->where('date', $date)->where('auction', 1)->first();
                            $formatTypeData[1][] = $typeItem;

                            $typeItem = $typeValues->where('date', $date)->where('auction', 2)->first();
                            $formatTypeData[2][] = $typeItem;
                        }else{
                            $typeItem = $typeValues->where('date', $date)->where('auction', $auction_code)->first();
                            $formatTypeData[1][] = $this->fillChartPoint($typeItem, $y, $m, $date);
                        }

                        dd($formatTypeData);
                        if ($m == 12) {
                            $y++;
                            break;
                        }
                        if ($y == $dates['to']['year'] && $m == $dates['to']['month']) {
                            break 2;
                        }
                    }
                }
            } else {
                $formatTypeData = $typeValues->toArray();
            }
        }

        return $formatTypeData;
    }

    public function getAllAuctionNames($auction_id) : array
    {
        $auctionNames = $this->repository->getAllAuctionNames($auction_id);
        return $this->auctionNameVerification($auctionNames, $auction_id);
    }

    public function auctionNameVerification($auctionNames, $auction_id) : array
    {
        $auctionData = [];
        if ($auctionNames->count()){
                if ($auction_id == 1 || $auction_id == 2){
                    $auctionItem = $auctionNames->where('id', 1)->first();
                    $auctionData[1][] =  $auctionItem;
                    $auctionItem = $auctionNames->where('id', 2)->first();
                    $auctionData[2][] =  $auctionItem;
                }else{
                    $auctionItem = $auctionNames->where('id', $auction_id)->first();
                    $auctionData[1][] =  $auctionItem;
                }
        }
        return $auctionData;
    }

    public function show(string $make, string $model, int $year, array $dates, int $auction_code): array
    {
        $chartData = $this->repository->show($make, $model, $year, $this->formatDates($dates), $auction_code);
        return $this->formatChartData($chartData, $dates, $auction_code, $year);
    }

    private function formatDates($dates): array
    {
        return [
            'dateFrom' => $dates['from']['year'] . $this->formatMonth($dates['from']['month']),
            'dateTo'   => $dates['to']['year'] . $this->formatMonth($dates['to']['month'])
        ];
    }

    private function formatChartData($chartData, $dates, $auction_code, $year): array
    {
        $formatChartData = [];

        if ($chartData->count()) {
            if ($chartData->count() != 24) {
                for ($y = $dates['from']['year']; $y <= $dates['to']['year'];) {
                    $startMonth = $y == $dates['from']['year'] ? $dates['from']['month'] : 1;

                    for ($m = $startMonth; $m <= 12; $m++) {
                        $date = $y . $this->formatMonth($m);

                        if ($auction_code == 1 || $auction_code == 2 ){
                            $chartItem = $chartData->where('date', $date)->where('auction', 1)->first();
                            $formatChartData[1][] = $this->fillChartPoint($chartItem, $y, $m, $date, $year);

                            $chartItem = $chartData->where('date', $date)->where('auction', 2)->first();
                            $formatChartData[2][] = $this->fillChartPoint($chartItem, $y, $m, $date, $year);
                        }else{
                            $chartItem = $chartData->where('date', $date)->where('auction', $auction_code)->first();
                            $formatChartData[1][] = $this->fillChartPoint($chartItem, $y, $m, $date, $year);
                        }

                        if ($m == 12) {
                            $y++;
                            break;
                        }
                        if ($y == $dates['to']['year'] && $m == $dates['to']['month']) {
                            break 2;
                        }
                    }
                }
            } else {
                $formatChartData = $chartData->toArray();
            }
        }

        return $formatChartData;
    }

    private function formatMonth($month): string
    {
        return ($month < 10 ? '0' : '') . $month;
    }

    private function fillChartPoint($chartItem, $y, $m, $date, $year)
    {
        if ($chartItem) {
            return $chartItem->toArray();
        } else {
            return [
                'statistic_year'  => $y,
                'statistic_month' => $m,
                'date'            => $date,
                'max_bid'         => 0,
                'min_bid'         => 0,
                'middle_bid'      => 0,
                'count_sales'     => 0,
                'car_year'     => $year
            ];
        }
    }
}
