<?php

namespace App\Services\ChartCarSales\Repositories;

interface ChartCarSalesRepositoryInterface
{
    public function show(string $make, string $model, int $year, array $dates, int $auction_code);
    public function getAllAuctionNames( array $auction_id);
    public function getAllTypeValues( string $make, string $model, int $year, array $dates, string $type);
}
